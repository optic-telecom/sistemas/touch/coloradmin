import React from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import Avatar from '../../user/Avatar';

// Services
import AuthenticationService from './../../../services/AuthenticationService'

class DropdownProfile extends React.Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			dropdownOpen: false
		};
	}

	toggle() {
		this.setState(prevState => ({
			dropdownOpen: !prevState.dropdownOpen
		}));
	}
  
	render() {
		return (
			<Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle} className="dropdown navbar-user" tag="li">
				<DropdownToggle tag="a">
					<Avatar /> 
					<span className="d-none d-md-inline">Jon Doe</span> <b className="caret"></b>
				</DropdownToggle>
				<DropdownMenu className="dropdown-menu dropdown-menu-right" tag="ul">
					<DropdownItem onClick={async () => AuthenticationService.login("josealvarado", "RjzupvcKL2THgJp")}>Autorización</DropdownItem>
					<DropdownItem>Configuración</DropdownItem>
					<div className="dropdown-divider"></div>
					<DropdownItem>Salir</DropdownItem>
				</DropdownMenu>
			</Dropdown>
		);
	}
};

export default DropdownProfile;
