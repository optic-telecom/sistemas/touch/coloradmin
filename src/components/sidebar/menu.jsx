const Menu = [
  { path: '/', icon: 'fa fa-th', title: 'Dashboard'},
  { path: '/nodos', icon: 'fa fa-code-branch', title: 'Nodos'},
  { path: '/olt', icon: 'fa fa-satellite-dish', title: 'OLT',
    children: [
      { path: '/olt/general', title: 'General'},
      { path: '/olt/provisionar', title: 'Provisionar ONU'},
    ]
  },
  { path: '/utp', icon: 'fa fa-satellite-dish', title: 'UTP',
    children: [
      { path: '/utp/lista', title: 'Lista de nodos UTP'},
      { path: '/utp/radius-server', title: 'Radius Server'},
      { path: '/utp/provisionar-clientes', title: 'Provisionar Clientes UTP'},
    ]
  },
  { path: '/cpes', icon: 'fa fa-table', title: 'CPEs',
    children: [
      { path: '/cpe/onu', title: 'ONU'},
      { path: '/cpe/utp', title: 'UTP'}
    ]
  },
  { path: '/fibra', icon: 'fa fa-network-wired', title: 'Fibra',
    children: [
      { path: '/fibra/extremos-fisicos', title: 'Extremos físicos'},
      { path: '/fibra/fabricantes', title: 'Fabricantes'},
      { path: '/fibra/mufas', title: 'Mufas'},
      { path: '/fibra/cabeceras', title: 'Cabeceras'}
    ]
	},
	{ path: '/', icon: 'fa fa-wrench', title: 'Configuraciones',
    children: [
			{ path: '/configuracion/ssh', title: 'SSH'},
			{ path: '/configuracion/velocidades', title: 'Velocidades',
        children: [
          { path: '/configuracion/velocidades/utp', title: 'UTP'}
        ]
      },
      { path: '/configuracion/planes', title: 'Planes' },
      { path: '/configuracion/operador', title: 'Operador' },
      { path: '/configuracion/vlan', title: 'VLANs del sistema' },
      { path: '/configuracion/django', title: 'Admin Django' },
    ]
  },
]

export default Menu;
