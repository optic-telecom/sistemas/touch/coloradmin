import React from 'react'

const Icon = ({fas=true, far=false, icon=null, animation=null, size=1, className}) => (
	<i className={`${fas && "fas"} ${far && "far"} ${icon && `fa-${icon}`} ${animation && `fa-${animation}`} ${size && `fa-${size}x`} ${className}`}></i>
);

export default Icon