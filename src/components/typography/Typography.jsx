import React from 'react'

export const PageTitle = props => (
	<h1 className="page-header">{props.title} <small>{props.subtitle}</small></h1>
);

export const Paragraph = props => (
	<p>{props.children}</p>
)