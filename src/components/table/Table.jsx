// Packages
import React, {useState} from 'react'
import styled from 'styled-components'
import {useTable, useSortBy, useFilters, usePagination, useTableState} from 'react-table'

// Components
import Button from './../../components/buttons/Button'

const TableOptions = styled.div`
	--margin: .5rem;
	display: flex;
	flex-flow: row nowrap;
	margin-bottom: calc(var(--margin) * 2);
	width: 100%;
	justify-content: space-between;
	align-items: center;
	.filter {
		display: flex;
		flex-flow: row nowrap;
		justify-content: flex-end;
		align-items: center;
	}
	.input-group {
		display: flex;
		padding: var(--margin);
		flex-flow: row nowrap;
		width: 100%;
		max-width: 250px;
		border-radius: 4px;
		border: 1px solid #ddd;
		input[type=text] {
			width: 90%;
			border: 0;
			background-none;
			outline: none;
		}
		.fas {
			margin-right: var(--margin);
			margin-left: var(--margin);
		}
	}
	.filter-toggle {
		margin-left: 1rem;
		font-size: 1rem;
	}
`
const TableColumnFilter = styled.input`
	width: 100%;
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: .25rem;
	outline: none;
`

const DefaultColumnFilter = ({
	column: { filterValue, setFilter, Header }
}) => {
	return (
		<TableColumnFilter
			value={filterValue || ''}
			onChange={e => {
				setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
			}}
		/>
	)
}

const Table = ({className, columns, data, buttons, pagination}) => {
	const [dataTable, filterDataTable] = useState(data)
	const [isTableAdvance, setTableAdvance] = useState(false)
	const tableState = useTableState({ pageIndex: 0, pageSize: 7 })

	const {
		getTableProps,
		headerGroups,
		prepareRow,
		page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
		setPageSize,
		state: [{ pageIndex, pageSize }],
	} = useTable(
		{
			data: dataTable,
			columns: columns.map(column => column = {...column, Filter: DefaultColumnFilter}),
			state: tableState
		},
		useFilters,
		useSortBy,
		usePagination
	)

	const filter = (e) => {
		const result = []
		const keys = Object.keys(data[0])
		data.map(_row => {
			let skip = false
			return keys.map(key => {
				if(`${_row[key]}`.toLowerCase().search(e.target.value.toLowerCase()) !== -1 && !skip) {
					skip = true
					result.push(_row)
				}
				return true
			})
		})

		return filterDataTable(result)
	}

	const toggleTableAdvance = () => setTableAdvance(!isTableAdvance)
	return (
		<>
		<TableOptions>
			<div className="buttons">
				{ buttons ? buttons.map(btn => btn) : null }
			</div>
			<div className="filter">
				<div className="input-group">
					<div className="input-group__icon">
						<span className="fas fa-search"></span>
					</div>
					<input type="text" onChange={filter}/>
				</div>
				<div className="filter-toggle">
					<Button color="default" onClick={toggleTableAdvance}>
						<span className="fas fa-sliders-h clickable"></span>
					</Button>
				</div>
			</div>
		</TableOptions>
		<table {...getTableProps()} className={`table dataTable ${className}`}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              // Add the sorting props to control sorting. For this example
              // we can add them into the header props
              <th {...column.getHeaderProps(column.getSortByToggleProps())}>
								{column.render('Header')}
                <span>
                  {/* Add a sort direction indicator */}
                  <span>
                    {column.isSorted
                      ? column.isSortedDesc
                        ? ' 🔽'
                        : ' 🔼'
                      : null}
                  </span>
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody>
				{
					isTableAdvance
					? <tr>{ headerGroups[0].headers.map(column => <td style={{ backgroundColor: '#f7f7f7' }}>{ column.render('Filter') }</td>) }</tr>
					: null
				}
        {page.map(
          (row, i) =>
            prepareRow(row) || (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                })}
              </tr>
            )
        )}
      </tbody>
    </table>
		<div className="pagination">
			{
				pagination
				? (
					<>
					<button onClick={() => pagination.previous()}>
						{'<'}
					</button>{' '}
					<button onClick={() => pagination.next()}>
						{'>'}
					</button>{' '}
					<span>
						Page{' '}
						<strong>
							{pageIndex + 1} of {pageOptions.length}
						</strong>{' '}
					</span>
					<span>
						| Go to page:{' '}
						<input
							type="number"
							defaultValue={pageIndex + 1}
							onChange={e => {
								const page = e.target.value ? Number(e.target.value) - 1 : 0
								gotoPage(page)
							}}
							style={{ width: '100px' }}
						/>
					</span> <select
						value={pageSize}
						onChange={e => {
							setPageSize(Number(e.target.value))
						}}
					>
						{[10, 20, 30, 40, 50].map(pageSize => (
							<option key={pageSize} value={pageSize}>
								Show {pageSize}
							</option>
						))}
					</select>
					</>
				)
				: (
					<>
					<button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
						{'<<'}
					</button>{' '}
					<button onClick={() => previousPage()} disabled={!canPreviousPage}>
						{'<'}
					</button>{' '}
					<button onClick={() => nextPage()}>
						{'>'}
					</button>{' '}
					<button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
						{'>>'}
					</button>{' '}
					<span>
						Page{' '}
						<strong>
							{pageIndex + 1} of {pageOptions.length}
						</strong>{' '}
					</span>
					<span>
						| Go to page:{' '}
						<input
							type="number"
							defaultValue={pageIndex + 1}
							onChange={e => {
								const page = e.target.value ? Number(e.target.value) - 1 : 0
								gotoPage(page)
							}}
							style={{ width: '100px' }}
						/>
					</span> <select
						value={pageSize}
						onChange={e => {
							setPageSize(Number(e.target.value))
						}}
					>
						{[10, 20, 30, 40, 50].map(pageSize => (
							<option key={pageSize} value={pageSize}>
								Show {pageSize}
							</option>
						))}
					</select>
					</>
				)
			}
		</div>
		</>
	)
}

export default Table