import React from 'react'

import Button from './../buttons/Button'

export const ModalHeader = ({children, close}) => (
	<div className="modal-header">
		<h5>{ children }</h5>
		<Button className="close" onClick={close}>x</Button>
	</div>
)

export const ModalBody = ({children}) => (
	<div className="modal-body">{ children }</div>
)

export const ModalFooter = ({children, close}) => (
	<div className="modal-footer">
		<Button color="white" onClick={close}>Cerrar</Button>
		{ children }
	</div>
)

export const Modal = ({children, isOpen}) => (
	<>
	{
		isOpen
		? (
			<>
			<div className={`modal fade show`} style={{ display: 'block' }}>
				<div className="modal-dialog">
					<div className="modal-content">
						{children}
					</div>
				</div>
			</div>
			<div className={`modal-backdrop fade show`}></div>
			</>
		)
		: null
	}
	</>
);