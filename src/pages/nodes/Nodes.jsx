// Packages
import React, { useState, useEffect } from 'react'

// Components
import Button from './../../components/buttons/Button'
import {Panel, PanelHeader, PanelBody} from './../../components/panel/panel'
import Breadcrumb from './../../components/breadcrumb/Breadcrumb'
import {PageTitle} from './../../components/typography/Typography'
import Table from './../../components/table/Table'
import {Modal, ModalHeader, ModalBody, ModalFooter} from './../../components/modal/Modal'
import Spinner from '../../components/icons/Spinner'

// Services
import NodeService from '../../services/NodeService'

const columns = [
	{
		Header: "Nombre",
		accessor: "name"
	},
	{
		Header: "Dirección",
		id: "address",
		accessor: d => d.address
	},
	{
		Header: "Comunidad",
		accessor: "community"
	},
	{
		Header: "Equipos de red",
		accessor: "devices"
	},
	{
		Header: "Equipos CPEs",
		accessor: "devices_cpes"
	}
]

const Nodes = props => {
	const [data, fetchData] = useState([])
	const [isLoaded, load] = useState(false)
	const [isOpenModal, setOpenModal] =  useState(false)
	const toggleOpenModal = () => setOpenModal(!isOpenModal)
	const CreateNodeButton = <Button key={1} color="success" size="sm" onClick={toggleOpenModal}>Crear un Nodo</Button>

	useEffect((isLoaded) => {
		if(!isLoaded) {
			(async () => {
				const result = await NodeService.getByPage()
				fetchData(result)
				load(true)
			})()
		}
	}, [])

	return (
		<>
		<Breadcrumb />
		<PageTitle title="Nodos" />
		<div className="row">
			<div className="col-xs-12">
				<Panel>
					<PanelHeader noButton>Lista de nodos</PanelHeader>
					<PanelBody>
						{
							isLoaded
							? <Table columns={columns} data={data} buttons={[CreateNodeButton]} />
							: <Spinner />
						}
					</PanelBody>
				</Panel>
			</div>
		</div>
		<Modal isOpen={isOpenModal}>
			<ModalHeader>Agregar Nodo</ModalHeader>
			<ModalBody>
				<form action="">
					<div className="form-group">
						<label className="col-form-label" htmlFor="Nombre">Nombre</label>
						<input className="form-control" placeholder="Nombre del nodo" />
					</div>
					<div className="form-group">
						<label className="col-form-label" htmlFor="Direccion">Direccion</label>
						<input className="form-control" placeholder="Direccion del nodo" />
					</div>
					<div className="form-group">
						<label className="col-form-label" htmlFor="Comunidad">Comunidad</label>
						<input className="form-control" placeholder="Comunidad donde se encuentra el nodo" />
					</div>
				</form>
			</ModalBody>
			<ModalFooter close={toggleOpenModal}>
				<Button color="success">Crear</Button>
			</ModalFooter>
		</Modal>
		</>
	)
}

export default Nodes