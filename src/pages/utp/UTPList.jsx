// Packages
import React from 'react'

// Components
import Button from './../../components/buttons/Button'
import {Panel, PanelHeader, PanelBody} from './../../components/panel/panel'
import Breadcrumb from './../../components/breadcrumb/Breadcrumb'
import {PageTitle} from './../../components/typography/Typography'
import Table from './../../components/table/Table'

const data = [
  {
    "id": 1,
    "name": "Nodo 1",
    "devices": 0,
    "devices_cpes": 0,
    "address": "dire",
    "community": "comunidad"
  },
  {
    "id": 2,
    "name": "Nodo 2",
    "devices": 0,
    "devices_cpes": 0,
    "address": "dire",
    "community": "comunidad"
  },
  {
    "id": 3,
    "name": "Nodo 3",
    "devices": 291,
    "devices_cpes": 0,
    "address": "dire",
    "community": "comunidad"
  },
  {
    "id": 4,
    "name": "Nodo 4",
    "devices": 224,
    "devices_cpes": 4,
    "address": "dire",
    "community": "comunidad"
  },
  {
    "id": 5,
    "name": "Nodo 5",
    "devices": 48,
    "devices_cpes": 0,
    "address": "dire",
    "community": "comunidad"
  }
]

const columns = [
	{
		Header: "Nombre",
		accessor: "name"
	},
	{
		Header: "Dirección",
		id: "address",
		accessor: d => d.address
	},
	{
		Header: "Comunidad",
		accessor: "community"
	},
	{
		Header: "Equipos de red",
		accessor: "devices"
	},
	{
		Header: "Equipos CPEs",
		accessor: "devices_cpes"
	}
]

const UTPList = props => {
	const CreateNodeButton = <Button color="success" size="sm">Crear nuevo nodo UTP</Button>
	return (
		<>
		<Breadcrumb />
		<PageTitle title="Lista de nodos UTP" />
		<div className="row">
			<div className="col-xs-12">
				<Panel>
					<PanelHeader noButton>Lista de nodos UTP</PanelHeader>
					<PanelBody>
						<Table columns={columns} data={data} buttons={[CreateNodeButton]} />
					</PanelBody>
				</Panel>
			</div>
		</div>
		</>
	)
}

export default UTPList