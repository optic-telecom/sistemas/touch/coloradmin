// Packages
import React, {useState, useEffect} from 'react'

// Components
import {Panel, PanelHeader, PanelBody} from './../../components/panel/panel'
import Breadcrumb from './../../components/breadcrumb/Breadcrumb'
import {PageTitle} from './../../components/typography/Typography'
import Table from './../../components/table/Table'
import Spinner from './../../components/icons/Spinner'

// Services
import OnuService from './../../services/OnuService'

const columns = [
	{
		Header: "ID",
		accessor: "id"
	},
	{
		Header: "Numero de Servicio",
		id: "service_number",
		accessor: d => d.service_number
	},
	{
		Header: "Estado",
		accessor: "status"
	},
	{
		Header: "SN",
		accessor: "serial"
	},
	{
		Header: "Modelo",
		accessor: "model"
	},
	{
		Header: "Descripción",
		accessor: "description"
	},
	{
		Header: "Distancia",
		accessor: "distance"
	},
	{
		Header: "Señal Optica",
		accessor: "RX"
	},
	{
		Header: "MAC",
		accessor: "mac"
	},
	{
		Header: "IP",
		accessor: "ip"
	}
]

const ONUList = props => {
	const [data, fetchData] = useState([])
	const [page, setPage] = useState(1)
	const [isLoaded, load] = useState(false)

	useEffect((isLoaded) => {
		if(!isLoaded && data.length === 0) {
			(async () => {
				const response = await OnuService.getAll()
				fetchData(response)
				load(true)
			})()
		}
	})

	const next = () => {
		(async () => {
			const response = await OnuService.getByPage(page+1)
			fetchData(response)
			setPage(page+1)
			console.log(response)
		})()
	}

	const previous = () => {
		(async () => {
			if(page > 1) {
				const response = await OnuService.getByPage(page-1)
				fetchData(response)
				setPage(page-1)
			}
		})()
	}

	return (
		<>
		<Breadcrumb />
		<PageTitle title="Lista de ONU" />
		<div className="row">
			<div className="col-xs-12">
				<Panel>
					<PanelHeader noButton>Lista de ONU</PanelHeader>
					<PanelBody>
					{
						isLoaded
						? <Table
								columns={columns}
								data={data}
								pagination={{
									page,
									next,
									previous
								}}
							/>
						: <Spinner />
					}
					</PanelBody>
				</Panel>
			</div>
		</div>
		</>
	)
}

export default ONUList