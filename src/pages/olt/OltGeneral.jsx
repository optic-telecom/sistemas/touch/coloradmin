import React, {useState, useEffect} from 'react'

// Components
import Table from './../../components/table/Table'
import Button from './../../components/buttons/Button'
import {Panel, PanelHeader, PanelBody} from './../../components/panel/panel'
import Breadcrumb from './../../components/breadcrumb/Breadcrumb'
import {PageTitle} from './../../components/typography/Typography'
import Spinner from './../../components/icons/Spinner'

// Services
import OltService from './../../services/OltService'

const columns = [
	{
		Header: "OLT ID",
		id: "id",
		accessor: "id"
	},
	{
		Header: "Alias",
		accessor: "alias"
	},
	{
		Header: "Dirección IP",
		accessor: "ip"
	},
	{
		Header: "ONUs",
		accessor: "onus"
	},
	{
		Header: "Uptime",
		accessor: "uptime"
	},
	{
		Header: "Modelo",
		accessor: d => d.model.model
	},
	{
		Header: "Descripción",
		accessor: "description"
	},
	{
		Header: "Acciones",
		Call: item => <button className="button btn-xs btn-danger">Remover</button>
	}
]

const CreateOLTButton = <Button key={1} color="success" size="sm">Create OLT Button</Button>

const OltGeneral = props => {
	const [data, fetchData] = useState([])
	const [page, setPage] = useState(1)
	const [isLoaded, load] = useState(false)

	useEffect((isLoaded) => {
		if(!isLoaded) {
			(async () => {
				const response = await OltService.getAll()
				fetchData(response)
				load(true)
			})()
		}
	}, [])

	const next = () => {
		(async () => {
			const response = await OltService.getByPage(page+1)
			fetchData(response)
			setPage(page+1)
		})()
	}

	const previous = () => {
		(async () => {
			if(page > 1) {
				const response = await OltService.getByPage(page-1)
				fetchData(response)
				setPage(page-1)
			}
		})()
	}

	return (
		<>
		<Breadcrumb />
		<PageTitle title="OLT General" />
		<div className="row">
			<div className="col-xs-12">
				<Panel>
					<PanelHeader noButton>Lista de olt</PanelHeader>
					<PanelBody>
						{
							isLoaded
							? <Table
									columns={columns}
									data={data}
									buttons={[CreateOLTButton]}
									pagination={{
										page,
										next,
										previous
									}}
								/>
							: <Spinner />
						}
					</PanelBody>
				</Panel>
			</div>
		</div>
		</>
	)
}

export default OltGeneral