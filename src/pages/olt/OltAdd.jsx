// Packages
import React from 'react'

// Components
import Button from './../../components/buttons/Button'
import {Panel, PanelHeader, PanelBody} from './../../components/panel/panel'
import Breadcrumb from './../../components/breadcrumb/Breadcrumb'
import {PageTitle, Paragraph} from '../../components/typography/Typography'

const OltAdd = props => {
	return (
		<>
		<Breadcrumb />
		<PageTitle title="Provisionar OLT" />
		<div className="row">
			<div className="col-xs-12">
				<Panel>
					<PanelHeader noButton>Provisionar OLT</PanelHeader>
					<PanelBody>
						<Paragraph>Para buscar en una sola OLT escribe el alias de la misma y seleccionala de la lista desplegable.</Paragraph>
						<div className="row">
							<div className="col-md-6">
								<form>
									Seleccionar por OLT
									<div className="form-group">
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="" aria-label="Example text with button addon" aria-describedby="button-addon1" />
											<div class="input-group-append">
												<Button size="sm" color="success" id="button-addon1">Descubrir ONUs todas las OLTs</Button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</PanelBody>
				</Panel>
			</div>
		</div>
		</>
	)
}

export default OltAdd