import React from 'react';

import Home from './../pages/home/Home';
import Nodes from './../pages/nodes/Nodes';
import OltGeneral from '../pages/olt/OltGeneral';
import OltAdd from '../pages/olt/OltAdd';
import UTPList from '../pages/utp/UTPList';
import RadiusServer from '../pages/utp/RadiusServer';
import UTPClients from '../pages/utp/UTPClients';
import ONUList from '../pages/cpes/ONUList';
import CPEUTPList from '../pages/cpes/UTPList';

const routes = [
  {
    path: '/',
    exact: true,
    component: () => <Home />
  },
  {
    path: '/nodos',
    title: 'Nodos',
    component: () => <Nodes />,
  },
  {
    path: '/olt/general',
    title: 'Olt General',
    component: () => <OltGeneral />,
	},
	{
    path: '/olt/provisionar',
    title: 'Olt Provisionar',
    component: () => <OltAdd />,
  },
	{
    path: '/utp/lista',
    title: 'Lista de Nodos UTP',
    component: () => <UTPList />,
  },
	{
    path: '/utp/radius-server',
    title: 'Radius Server',
    component: () => <RadiusServer />,
  },
	{
    path: '/utp/provisionar-cliente',
    title: 'Provisionar clientes',
    component: () => <UTPClients />,
	},
	{
    path: '/cpe/onu',
    title: 'CPE Lista de ONU',
    component: () => <ONUList />,
  },
	{
    path: '/cpe/utp',
    title: 'CPE Lista de UTP',
    component: () => <CPEUTPList />,
  }
];


export default routes;