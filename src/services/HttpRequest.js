import axios from 'axios';
import Config from '../config';

class HttpRequest {
	constructor() {
		this.config = this.config.bind(this)
		this.get = this.get.bind(this)
		this.post = this.post.bind(this)
		this.put = this.put.bind(this)
		this.delete = this.delete.bind(this)
	}

	static config() {
		return {
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `jwt ${sessionStorage.getItem('authorization')}`
			}
		}
	}


	static async get(id = null) {
		let request

		if(!id) {
			try {
				request = await axios.get(`${Config.API_URL}/${this.endpoint}`, this.config());
				return request.data
			} catch (error) {
				console.log(error.message)
			}
		}
		else {
			try {
				request = await axios.get(`${Config.API_URL}/${this.endpoint}/${id}`, this.config());
				return request.data
			} catch (error) {
				console.log(error.message)
			}
		}
	}

	static async post(payload) {
		const request = await axios.post(`${Config.API_URL}/${this.endpoint}`, payload, this.config());
		return request.data;
	}
	
	static async put(id, payload) {
		const request = await axios.post(`${Config.API_URL}/${this.endpoint}/${id}`, payload, this.config());
		return request.data;
	}
	
	static async delete(id) {
		const request = await axios.post(`${Config.API_URL}/${this.endpoint}/${id}`, this.config());
		return request.data;
	}
}

export default HttpRequest;