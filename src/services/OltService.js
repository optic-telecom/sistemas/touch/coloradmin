import HttpRequest from './HttpRequest';

const route = "olt"

class OltService extends HttpRequest {
	static endpoint = route

	static async getAll() {
		const response = await this.get()
		return response
	}

	static async getByPage(page=1) {
		this.endpoint = `${route}/?page=${page}`
		const response = await this.get()
		return response
	}
}

export default OltService;