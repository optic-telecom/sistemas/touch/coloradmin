import HttpRequest from './HttpRequest';

class AuthenticationService extends HttpRequest {
	static endpoint = "auth"

	static async login(username, password) {
		this.endpoint = "auth/obtain_token/"
		const response = await this.post({username, password})
		sessionStorage.setItem('authorization', response.token)
	}
}

export default AuthenticationService;