import HttpRequest from './HttpRequest'

const route = "datatable"

class PaginationService extends HttpRequest {
	static async getPagination(endpoint) {
		this.endpoint = `${route}/${endpoint}`
		const response = this.post()
		return response
	}
}