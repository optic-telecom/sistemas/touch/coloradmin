import HttpRequest from './HttpRequest';

class UserService extends HttpRequest {
	static endpoint = "user"

	static async getUser() {
		await this.get()
	}
}

export default UserService;